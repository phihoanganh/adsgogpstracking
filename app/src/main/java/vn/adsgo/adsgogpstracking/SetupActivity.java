package vn.adsgo.adsgogpstracking;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import vn.adsgo.adsgogpstracking.global.GlobalDefine;
import vn.adsgo.adsgogpstracking.service.GpsService;
import vn.adsgo.adsgogpstracking.utils.SharedPreferencesUtils;

/**
 * Created by MacBookPro on 11/6/18.
 */

public class SetupActivity extends AppCompatActivity {
    private EditText etLicensePlate, etOldPass, etNewPass, etRetypePass;
    private Button btnSave, btnChangePass;
    private String licensePlate;
    private TextView tvLicensePlate;
    private LinearLayout llChangePass, llSetup;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        licensePlate = SharedPreferencesUtils.getInstance(SetupActivity.this).getString(GlobalDefine.KEY_SAVE_LICENSE_PLATE);
        etLicensePlate = findViewById(R.id.etLicensePlate);
        etOldPass = findViewById(R.id.etOldPass);
        etNewPass = findViewById(R.id.etNewPass);
        etRetypePass = findViewById(R.id.etRetypePass);
        btnChangePass = findViewById(R.id.btnChangePass);
        llChangePass = findViewById(R.id.llChangePass);
        llSetup = findViewById(R.id.llSetup);
        tabLayout = findViewById(R.id.tabLayout);

        btnSave = findViewById(R.id.btnSave);
        tvLicensePlate = findViewById(R.id.tvLicensePlate);
        if (licensePlate != null) {
            tvLicensePlate.setText(licensePlate);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                licensePlate = etLicensePlate.getText().toString();
                licensePlate = licensePlate.replaceAll("\\W", "").toUpperCase();
                if(TextUtils.isEmpty(licensePlate)){
                    Toast.makeText(SetupActivity.this, "Vui lòng nhập biển số", Toast.LENGTH_LONG).show();
                    return;
                }else{
                    SharedPreferencesUtils.getInstance(SetupActivity.this).setString(GlobalDefine.KEY_SAVE_LICENSE_PLATE, licensePlate);
                    tvLicensePlate.setText(licensePlate);
                    Intent autoRestart = new Intent(SetupActivity.this, GpsService.class);
                    autoRestart.putExtra("licensePlate", licensePlate);
                    startService(autoRestart);
                    Toast.makeText(SetupActivity.this, "Cài biển số thành công!", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPass = etOldPass.getText().toString();
                String newPass = etNewPass.getText().toString();
                String retypePass = etRetypePass.getText().toString();
                if (TextUtils.isEmpty(oldPass) || TextUtils.isEmpty(newPass) || TextUtils.isEmpty(retypePass)) {
                    Toast.makeText(SetupActivity.this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_LONG).show();
                } else if (!newPass.equals(retypePass)) {
                    Toast.makeText(SetupActivity.this, "Mật khẩu mới không trùng khớp", Toast.LENGTH_LONG).show();
                } else {
                    btnChangePass.setVisibility(View.INVISIBLE);
                    Map<String, String> postData = new HashMap<>();
                    postData.put("oldPassword", oldPass);
                    postData.put("newPassword", newPass);
                    HttpPostAsyncTask task = new HttpPostAsyncTask(postData);
                    task.execute("https://msc.adsgo.vn/adsgo-service/client/change-password");

                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    llSetup.setVisibility(View.VISIBLE);
                    llChangePass.setVisibility(View.GONE);
                } else {
                    llSetup.setVisibility(View.GONE);
                    llChangePass.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public class HttpPostAsyncTask extends AsyncTask<String, Void, String> {
        // This is the JSON body of the post
        JSONObject postData;

        // This is a constructor that allows you to pass in the JSON body
        public HttpPostAsyncTask(Map<String, String> postData) {
            if (postData != null) {
                this.postData = new JSONObject(postData);
            }
        }

        // This is a function that we are overriding from AsyncTask. It takes Strings as parameters because that is what we defined for the parameters of our async task
        @Override
        protected String doInBackground(String... params) {

            try {
                // This is getting the url from the string we passed in
                URL url = new URL(params[0]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("jwt-token", SharedPreferencesUtils.getInstance(SetupActivity.this).getString(GlobalDefine.KEY_SAVE_TOKEN));

                urlConnection.setRequestMethod("PUT");


                // OPTIONAL - Sets an authorization header
                urlConnection.setRequestProperty("Authorization", "someAuthString");

                // Send the post body
                if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();
                if (statusCode == 200) {
                    return "ok";
                }

            } catch (Exception e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            btnChangePass.setVisibility(View.VISIBLE);
            if (s != null) {
                Toast.makeText(SetupActivity.this, "Đổi mật khẩu thành công", Toast.LENGTH_LONG).show();
                Intent pushIntent = new Intent(SetupActivity.this, MainActivity.class);
                pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(pushIntent);
            } else {
                Toast.makeText(SetupActivity.this, "Đổi mật khẩu không thành công", Toast.LENGTH_LONG).show();
            }
        }
    }

}
