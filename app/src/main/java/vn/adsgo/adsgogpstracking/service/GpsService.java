package vn.adsgo.adsgogpstracking.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import vn.adsgo.adsgogpstracking.MainActivity;
import vn.adsgo.adsgogpstracking.global.GlobalDefine;
import vn.adsgo.adsgogpstracking.rabbitmq.RabbitMq;
import vn.adsgo.adsgogpstracking.utils.SharedPreferencesUtils;

/**
 * Created by MacBookPro on 11/1/18.
 */

public class GpsService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private LocationManager locationManager, locationGPSManager;
    private CountDownTimer countDownTimer;
    private String licensePlate = null;
    private Location lastLocation;
    private RabbitMq rabbitMq;
    private WifiManager.WifiLock wifiLock;

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Rabbitmq", "start GPS_PROVIDER");
        if (licensePlate == null)
            licensePlate = SharedPreferencesUtils.getInstance(GpsService.this).getString(GlobalDefine.KEY_SAVE_LICENSE_PLATE);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationGPSManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        rabbitMq = RabbitMq.getInstance();
        createTimer();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, locationListener);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationGPSManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, locationListener);
        }
        preventInternetGoDown();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                Log.d("Rabbitmq", "uncaughtException");
                restartService();
            }
        });
// else {
////            Log.d("GPSTracking", "start NETWORK_PROVIDER");
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, locationListener);
//        }
    }

    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            Log.d("Rabbitmq", "update " + location.getLatitude() + "," + location.getLongitude());
            lastLocation = location;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    private void preventInternetGoDown() {
        wifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "mylock");
        wifiLock.acquire();
    }

    private void createTimer() {
        if (countDownTimer != null) return;
        countDownTimer = new CountDownTimer(15000, 5000) {
            public void onTick(long millisUntilFinished) {
                EnableGPSAutoMatically();
            }

            @SuppressLint("MissingPermission")
            public void onFinish() {
                if (lastLocation == null) {
                    lastLocation = locationGPSManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (lastLocation == null)
                        lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                sendGPS(lastLocation);
                countDownTimer = null;
                createTimer();
            }
        }.start();
    }

    private void sendGPS(Location location) {
        if (location == null || licensePlate == null) return;
        try {
            JSONObject json = new JSONObject();
            json.put("latitude", location.getLatitude());
            json.put("longitude", location.getLongitude());
            json.put("licenseplace", licensePlate);
            json.put("timelog", location.getTime());
            String message = json.toString();
            rabbitMq.publishMessage(message, rabbitMq.EXCHANGE_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Intent pushIntent = new Intent(GpsService.this, MainActivity.class);
                            pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(pushIntent);
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Intent pushIntent1 = new Intent(GpsService.this, MainActivity.class);
                            pushIntent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(pushIntent1);
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        restartService();
        Log.d("Rabbitmq", "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            String value = intent.getStringExtra("licensePlate");
            if (!TextUtils.isEmpty(value)) licensePlate = value;
        } catch (NullPointerException ex) {

        } catch (Exception e) {
        }
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("Rabbitmq", "onTaskRemoved");
        restartService();
    }

    private void restartService() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
//        if (wifiLock != null) {
//            wifiLock.release();
//        }
        rabbitMq.closeConnection();
        long ct = System.currentTimeMillis(); //get current time
        Intent restartService = new Intent(getApplicationContext(),
                GpsService.class);
        restartService.putExtra("licensePlate", licensePlate);
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 0, restartService,
                0);

        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.setRepeating(AlarmManager.RTC_WAKEUP, ct, 1 * 1000, restartServicePI);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
