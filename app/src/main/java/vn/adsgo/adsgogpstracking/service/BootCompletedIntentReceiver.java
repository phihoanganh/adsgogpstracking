package vn.adsgo.adsgogpstracking.service;

/**
 * Created by MacBookPro on 9/18/18.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import vn.adsgo.adsgogpstracking.MainActivity;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
//    String ACTION_WATCHDOG_OF_SERVICE = "ACTION_WATCHDOG_OF_SERVICE";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent pushIntent = new Intent(context, MainActivity.class);
            pushIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(pushIntent);
//            Intent autoRestart = new Intent(context, GpsService.class);
//            context.startService(autoRestart);
        }
    }
}