package vn.adsgo.adsgogpstracking.rabbitmq;

/**
 * Created by MacBookPro on 9/22/18.
 */

public class Message {
    private String message;
    private String exchange;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}
