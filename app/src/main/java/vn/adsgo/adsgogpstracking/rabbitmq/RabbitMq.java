package vn.adsgo.adsgogpstracking.rabbitmq;

import android.location.Location;
import android.os.NetworkOnMainThreadException;
import android.util.Log;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AlreadyClosedException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;

/**
 * Created by MacBookPro on 9/4/18.
 */

public class RabbitMq {
    private ConnectionFactory factory;
    private Channel channel = null;
    public static Thread thread;
    private static RabbitMq instance = null;
    public static final String EXCHANGE_NAME = "adsgo-gps";
    public static BlockingDeque<Message> myQueue = new LinkedBlockingDeque<>();

    private Connection connection = null;

    public RabbitMq() {
        setupRabbit();
    }

    public static RabbitMq getInstance() {
        if (instance == null) {
            instance = new RabbitMq();
        }
        return instance;
    }
    /////////                     Listen from server side
//                    String queueName = channel.queueDeclare().getQueue();
//                    channel.queueBind(queueName, EXCHANGE_NAME, "");
//                    Consumer consumer = new DefaultConsumer(channel) {
//                        @Override
//                        public void handleDelivery(String consumerTag, Envelope envelope,
//                                                   AMQP.BasicProperties properties, byte[] body) throws IOException {
//                            String message = new String(body, "UTF-8");
//                            Log.d("Rabbitmq", " Received '" + envelope.getRoutingKey() + "':'" + message + "'");
//                        }
//                    };
//                    channel.basicConsume(queueName, true, consumer);
////////                    End listener


    private void setupRabbit() {
        factory = new ConnectionFactory();
        factory.setHost("54.169.151.6");
        factory.setPort(9090);
        factory.setUsername("testauto");
        factory.setPassword("12345678");
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    connection = factory.newConnection();
                    channel = connection.createChannel();
                    channel.confirmSelect();
                    if (!channel.isOpen()) {
                        channel = null;
                        return;
                    }
                    while (true) {
                        if (connection == null) {//|| !connection.isOpen()
                            closeConnection();
                            break;
                        }
                        Message message = null;
                        try {
                            message = myQueue.takeFirst();
                            if (!(message == null)) {
                                Log.d("Rabbitmq", " Sent '" + message.getExchange() + "':'" + message.getMessage() + "'");
                                channel.basicPublish(message.getExchange(), "", null, message.getMessage().getBytes("UTF-8"));
                                channel.waitForConfirmsOrDie();
                            }
                        } catch (InterruptedException e) {
                            Log.d("Rabbitmq", " Sent InterruptedException in");
                            putFirstQueue(message);
                            closeConnection();
//                            break;
                        } catch (ShutdownSignalException e) {
                            Log.d("Rabbitmq", " Sent ShutdownSignalException in");
                            putFirstQueue(message);
                            thread.sleep(14500); //sleep and then try again
//                            closeConnection();
//                            break;
                        } catch (Exception e) {
                            Log.d("Rabbitmq", " Sent Exception in");
                            putFirstQueue(message);
                            closeConnection();
//                            break;
                        }
                        thread.sleep(500);
                    }

                } catch (IOException e) {
                    Log.d("Rabbitmq", " Sent IOException out");
                    closeConnection();
                } catch (TimeoutException e) {
                    Log.d("Rabbitmq", " Sent TimeoutException out");
                    closeConnection();
                } catch (Exception e) {
                    Log.d("Rabbitmq", " Sent Exception out");
                    closeConnection();
                }
            }
        });
        thread.start();
    }

    private void putFirstQueue(Message message) {
        try {
            myQueue.putFirst(message);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public void clearQueue() {
        myQueue.clear();
    }

    public void closeConnection() {
        channel = null;
        connection = null;
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }

    public void publishMessage(String message, String exchangeS) {
        if (thread == null || !thread.isAlive()) {
            Log.d("Rabbitmq", "Reopen Connection");
            setupRabbit();
        }
        if (myQueue.size() > 500) {
            myQueue.removeFirst();
        }
        try {
            Message newMessage = new Message();
            newMessage.setMessage(message);
            newMessage.setExchange(exchangeS);
            myQueue.put(newMessage);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

}
